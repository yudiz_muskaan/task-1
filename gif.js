$(document).ready(function () {
    var refresh;
    const duration = 1000 * 10;
   
    const giphy = {
        baseURL: "https://api.giphy.com/v1/gifs/",
        apiKey: "9Pd2qKFu9JEE7JIkjqtbMpmHviQ4Jq0d",
        tag: "fail",
        type: "random",
        rating: "pg-13"
    };
    const $gif_wrap = $("#gif-wrap");
    // Giphy API URL
    let giphyURL = encodeURI(
        giphy.baseURL +
        giphy.type +
        "?api_key=" +
        giphy.apiKey +
        "&tag=" +
        giphy.tag +
        "&rating=" +
        giphy.rating
    );

    var newGif = () => $.getJSON(giphyURL, json => renderGif(json.data));

    var renderGif = _giphy => {
        console.log(_giphy);
        // Set gif as bg image
        $gif_wrap.css({
            "background-image":'url("'+_giphy.images.original.url + '")'
        });

        refreshRate();
    };

    // Call for new gif after duration
    var refreshRate = () => {
     // Reset set intervals
     clearInterval(refresh);
     refresh = setInterval(function() {
         // Call Giphy API for new gif
         newGif();
     }, duration);
    };
    newGif();
});