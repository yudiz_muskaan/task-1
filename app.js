//Auth, Profile
var errorMessage = '';
//1. name validation
function nameValidation(regName){
    let returnValue = true;

    if(regName === '' || regName.length < 6 ){
        returnValue = false;
        errorMessage = 'Fill Correct Details in Name';
        alert(errorMessage);
    }
    if(regName.length > 20){
        returnValue = false;
        errorMessage ='Enter Firstname and Lastname';
        alert(errorMessage) 
    }
    if(/^[A-Za-z\s]+$/.test(regName) === false){
        returnValue = false;
        errorMessage = 'Enter Only Characters';
        alert(errorMessage);
    }
    return returnValue;
}

//2. email validation
function emailValidation(regEmail){
    let returnValue = true;

    if(regEmail === '' || regEmail.length < 1 || regEmail.length < 6 ){
        returnValue = false;
        errorMessage = 'Fill Correct Details in Email';
        alert(errorMessage);
    }
    
    if(regEmail.length > 30){
        errorMessage ='Email is too long';
        alert(errorMessage);
    }
    
    if (/^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3})+$/.test(regEmail) === false){
        errorMessage = 'not a valid email';
        alert(errorMessage);
    }
    return returnValue;

}

// mobile validation
function mobileValidation(regMobile){
    let returnValue = true;
    
    if(regMobile.toString().length === '' || regMobile.toString().length < 10 || regMobile.toString().length > 10 ){
        returnValue = false;
        errorMessage = 'Enter 10 Digit Mobile Number';
        alert(errorMessage);
    }
  
    if (isNaN(regMobile) === true){ 
        errorMessage = 'Enter a Valid Number';
        alert(errorMessage);
    }
    return returnValue;
}

// gender validation
function genderValidation(regGender){
    let returnValue = true;

    if(regGender !== 'F' && regGender !== 'M'){
        errorMessage = 'Please Select Your Gender';
        alert(errorMessage);
        returnValue = false;
    }
    return returnValue;
}

//password validation
function passwordValidation(regPassword) {
    let returnValue = true;

    if(regPassword.length < 8){
        errorMessage = 'Password cannot be less than 8 characters';
        alert(errorMessage);
        returnValue = false;
    }
    if(regPassword.length > 16){
        errorMessage = 'Password Must be of 16 Characters only';
        alert(errorMessage);
        returnValue = false;
    }
    if(!regPassword.match(/[A-Za-z0-9*_$@]/g)){
        errorMessage ='Password Must Be Of 16 Characters and have atleast 1 Capital Letter, 1 Small Letter, 1 Number, 1 Symbol';
        alert(errorMessage);
        returnValue = false;
    }
    return returnValue;
}

function registerationFormValidation() {
    let r_name = document.forms['registerationForm']['rname'].value;
    let email = document.forms['registerationForm']['remail'].value;
    let mobile = document.forms['registerationForm']['rmobile'].value;
    let gender = document.forms['registerationForm']['rgender'].value;
    let dob = document.forms['registerationForm']['rdob'].value;
    let password = document.forms['registerationForm']['rpassword'].value;

    
    const userRegisteration = {
        name : r_name,
        email : email,
        mobile : mobile,
        gender : gender,
        dob : dob,
        password : password,
    };
    
    nameValidation(userRegisteration.name);
    emailValidation(userRegisteration.email);
    mobileValidation(userRegisteration.mobile);
    genderValidation(userRegisteration.gender);
    passwordValidation(userRegisteration.password);

    if(userRegisteration.name == '' || userRegisteration.email == '' || userRegisteration.mobile == '' || userRegisteration.gender == '' || userRegisteration.password == ''){
        alert((Object.values(userRegisteration)));
        alert('Kindly fill all the details');
    }else{
        alert('registeration successfull')
        localStorage.setItem(`${userRegisteration.email}`, JSON.stringify(userRegisteration));
        window.location.href = 'index.html';
    }
return true;
}

// user login: email/mobile, password
function doLogin(){
    let user = document.forms['loginForm']['user'].value;
    let password = document.forms['loginForm']['lpassword'].value;
    let logEmail = JSON.parse(localStorage.getItem(user)); //fetching details from local

if(logEmail == null){
    alert('Enter Registered Details to Login');
}else{
    if(logEmail['email'] != user || logEmail['password'] != password){
        errorMessage = 'Invalid Email or Mobile Number and Password';
        alert(errorMessage);
    }
    else if(logEmail['email'] == user || logEmail['password'] == password){
        sessionStorage.setItem('currentSession', logEmail['email']);
        alert('details matched, login sucessfull.');
        window.location.href = 'dashboard.html';
    }  
}
    return true;
}

// user logout

function logout(){
    sessionStorage.clear();
    window.location.href = "index.html";
    return true;
}

// user profile view
function profileView(){
    let sessionEmail = sessionStorage.getItem('currentSession');
    let localStorageDetails = JSON.parse(localStorage.getItem(sessionEmail));

    document.getElementById("userProfileEmail").href = `mailto:${sessionEmail}`;
    document.getElementById("userProfileMobile").href = `tel:+91${localStorageDetails['mobile']}`;
    document.getElementById("userProfileEmail").innerHTML = `${sessionEmail}`;
    document.getElementById("userProfileMobile").innerHTML = `+91${localStorageDetails['mobile']}`;

    return true;
}

// user profile edit : name, email, mobile
function profileOnLoad(){
    let sessionEmail = sessionStorage.getItem('currentSession');
    let localStorageDetails = JSON.parse(localStorage.getItem(sessionEmail));

    document.forms['editProfileForm']['ename'].value = localStorageDetails['name'];
    document.forms['editProfileForm']['eemail'].value = localStorageDetails['email'];
    document.forms['editProfileForm']['emobile'].value = localStorageDetails['mobile'];

}

function profileEdit(){
    let sessionEmail = sessionStorage.getItem('currentSession');
    let localStorageDetails = JSON.parse(localStorage.getItem(sessionEmail));

    let editProfileName = document.forms['editProfileForm']['ename'].value;
    let editProfileEmail = document.forms['editProfileForm']['eemail'].value;
    let editProfileMobile = document.forms['editProfileForm']['emobile'].value;
    
    nameValidation(editProfileName);
    emailValidation(editProfileEmail);
    mobileValidation(editProfileMobile);

    localStorageDetails['name'] = editProfileName;
    localStorageDetails['email'] = editProfileEmail;
    localStorageDetails['mobile'] = editProfileMobile;
    localStorage.removeItem(sessionEmail);
    localStorage.setItem(editProfileEmail, JSON.stringify(localStorageDetails));

        alert("Data updated Successfully. Kindly re-login to authenticate.");
        logout();
    
return true;
}

// user change password
function changePassword(){
    let getoldPassword = document.forms['changePasswordForm']['oldPassword'].value;
    let getnewPassword = document.forms['changePasswordForm']['newPassword'].value;
    let getconfirmPassword = document.forms['changePasswordForm']['confirmPassword'].value;
    
    if(!passwordValidation(getnewPassword)){
    return false;
    }else{
        let sessionEmail = sessionStorage.getItem('currentSession');
        let localStorageDetails = JSON.parse(localStorage.getItem(sessionEmail));


        if(getoldPassword == localStorageDetails['password']){
            if(getnewPassword == getconfirmPassword){
                localStorage.removeItem(sessionEmail);
                if(!localStorage.getItem(sessionEmail)){
                localStorageDetails['password'] = getnewPassword;
                localStorage.setItem(sessionEmail, JSON.stringify(localStorageDetails)); 
                alert('Password Changed Sucessfully');
                }
            }else{
                alert('New Password & Confirm Password Should Match');
            } 
        }else{
            alert('Please Enter correct registered Password');
        }
        return true;
    }
}


//Dashboard
// add task
var i = 1; 
function createChk(taskName) {
    if (taskName !== '') {
        
        // Create checkbox (its a input box of type checkbox).
        var chk;
        chk = document.createElement('input');  
        
        // Specify the type of element.
        chk.type = 'checkbox';
        chk.id = 'taskName' + i;  // Set an ID.
        chk.value = taskName;
        chk.name = 'task' + i;
        
        // Create label for checkbox.
        var lbl = document.createElement('label');  
        lbl.htmlfor = 'taskName' + i;
        lbl.appendChild(document.createTextNode(taskName));
        i = i+1;
    }else{
        document.getElementById(chk.id).focus();
    }     
    return chk;
}


var count = 0;
function addTask(){
    let getTask = document.forms['taskForm']['addTaskName'].value;
    let getChkData = createChk(getTask);
    let status = "pending";
    let taskData={
        name: getChkData.name,
        id: getChkData.id,
        value: getChkData.value,
        status: status
    };
   
    for (let j = 1; j <= 1; j++) {
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        count = count + 1;
        row.insertCell(0).innerHTML= count;
        row.insertCell(1).innerHTML= getTask;
        const input = document.createElement("input");
        input.type = "checkbox";
        input.name = taskData.name;
        input.id = taskData.id;
        input.value = taskData.value;
        let checkBoxColumn = row.insertCell(2);
        checkBoxColumn.appendChild(input);
        row.insertCell(3).innerHTML=`<a href="javascript:void(0);" onclick="updateTask(${taskData.id});">update</a>`;
        row.insertCell(4).innerHTML=`<a href="javascript:void(0);" onclick="deleteTask(${taskData.id});">Delete</a>`;

        if(localStorage.getItem(taskData.id)){
            let totalData = localStorage.length;
            let taskDataId = "taskName";
            let lastChar = (taskData.id).charAt((taskData.id).length-1);
            let idSum = Number(lastChar)+totalData;
            taskDataId += idSum;
            taskData.id = taskDataId;
            localStorage.setItem(taskDataId, JSON.stringify(taskData));


        }else{
        localStorage.setItem(input.id, JSON.stringify(taskData));
        }

        input.onclick = function(){
            let isChecked = document.getElementById(input.id);
            if ($(isChecked).is(":checked")) {
                taskData.status = "completed";
                localStorage.setItem(input.id, JSON.stringify(taskData));
            }else{
                taskData.status = "pending";
                localStorage.setItem(input.id, JSON.stringify(taskData));
            }
        }

    }
    location.href = "dashboard.html";
    return true;
}


// get todo task List
function taskList(){
    let count=0;
    //let taskDataId = "taskName";
    let arr = [];
    for(let i=0; i<localStorage.length; i++){
        if((localStorage.key(i)).match("taskName") == ''){
            continue;
        }else{
            if((localStorage.key(i)).match("taskName")){
                arr.push((localStorage.key(i)).match("taskName"));
            }
        var filtered = arr.filter(function (el) {
            return el != null;
        });
    } 
}
var variable = filtered.map(function(element){
    return element.input
})
var taskData = [];
for (let index = 0; index < variable.length; index++) {
  
    var ls = JSON.parse(localStorage.getItem(variable[index]));
    taskData[index] = {
        name: ls['name'],
        id: ls['id'],
        value: ls['value'],
        status: ls['status'],
    };
}

for (let j = 0; j < taskData.length; j++) {
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);
    count = count + 1;
    row.insertCell(0).innerHTML= count;
    row.insertCell(1).innerHTML= taskData[j]['value'];

    const input = document.createElement("input");
    if(taskData[j]['status'] == 'completed'){
        input.checked = true;
    }
    input.type = "checkbox";
    input.name = taskData[j]['name'];
    input.id = taskData[j]['id'];
    input.value = taskData[j]['value'];
    let checkBoxColumn = row.insertCell(2);
    checkBoxColumn.appendChild(input);
    row.insertCell(3).innerHTML=`<a href="javascript:void(0);" onclick="updateTask(${taskData[j]['id']});">update</a>`;
    row.insertCell(4).innerHTML=`<a href="javascript:void(0)" onclick="deleteTask(${taskData[j]['id']});">Delete</a>`;

    input.onclick = function(){
        let isChecked = document.getElementById(input.id);
        if ($(isChecked).is(":checked")) {
            taskData[j]['status'] = "completed";
            localStorage.setItem(input.id, JSON.stringify(taskData[j]));
        }else{
            taskData[j]['status'] = "pending";
            localStorage.setItem(input.id, JSON.stringify(taskData[j]));
        }
    }

}
    return true;
}


// update task
function updateTask(taskid){
    if(localStorage.getItem(taskid['id'])){
        let taskName = prompt("Please Enter Task Name");
        if(taskName.length < 1){
            alert("Please enter your task details.");
        }else{
            let localStore = JSON.parse(localStorage.getItem(taskid['id']));
            localStore['value'] = taskName;
            localStorage.setItem(taskid['id'], JSON.stringify(localStore));

            alert("Updated Sucessfully.");
            location.href = "dashboard.html";
        }
    }
}



// delete task
function deleteTask(taskid){
    if(localStorage.getItem(taskid['id'])){
        localStorage.removeItem(taskid['id']);
        alert("deleted successfully.");
        location.href = "dashboard.html";
    }
    else{
        alert("System is not able to delete.");
    }
}




// pending task
function pendingTaskList(){ 
    let count = 0;
    let taskDataId = "taskName";
    let arr = [];
    for(let i=0; i<localStorage.length; i++){
        if((localStorage.key(i)).match("taskName") == ''){
            continue;
        }else{
            if((localStorage.key(i)).match("taskName")){
                arr.push((localStorage.key(i)).match("taskName"));
            }
        var filtered = arr.filter(function (el) {
            return el != null;
        });
    } 
}
var variable = filtered.map(function(element){
    return element.input
})
var taskData = [];
for (let index = 0; index < variable.length; index++) {
  
    var ls = JSON.parse(localStorage.getItem(variable[index]));
    taskData[index] = {
        name: ls['name'],
        id: ls['id'],
        value: ls['value'],
        status: ls['status'],
    };
}
    for(let i=0; i < taskData.length;i++){
        var rowCount = table2.rows.length;
        var row = table2.insertRow(rowCount);
        if(localStorage.getItem(`${taskData[i]['id']}`) != null){

            if(taskData[i]['status'] == 'pending'){
                row.insertCell(0).innerHTML= count+=1;
                row.insertCell(1).innerHTML= taskData[i]['value'];
                row.insertCell(2).innerHTML= taskData[i]['status'];
            }
        }else{
            continue;
        }
    }
}

// completed task
function compeletedTaskList(){
    let count = 0;
    let taskDataId = "taskName";
    let arr = [];
    for(let i=0; i<localStorage.length; i++){
        if((localStorage.key(i)).match("taskName") == ''){
            continue;
        }else{
            if((localStorage.key(i)).match("taskName")){
                arr.push((localStorage.key(i)).match("taskName"));
            }
        var filtered = arr.filter(function (el) {
            return el != null;
        });
    } 
}
var variable = filtered.map(function(element){
    return element.input
})
var taskData = [];
for (let index = 0; index < variable.length; index++) {
  
    var ls = JSON.parse(localStorage.getItem(variable[index]));
    taskData[index] = {
        name: ls['name'],
        id: ls['id'],
        value: ls['value'],
        status: ls['status'],
    };
}
    for(let i=0; i < taskData.length;i++){
        var rowCount = table2.rows.length;
        var row = table2.insertRow(rowCount);
        if(localStorage.getItem(`${taskData[i]['id']}`) != null){

            if(taskData[i]['status'] == 'completed'){
                row.insertCell(0).innerHTML= count+=1;
                row.insertCell(1).innerHTML= taskData[i]['value'];
                row.insertCell(2).innerHTML= taskData[i]['status'];
            }
        }else{
            continue;
        }
    }
}

